﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WheelInfo : MonoBehaviour {

    public static WheelInfo instance;

    [System.Serializable]
    public class Info
    {
        public int wheelNo;
        public string wheelName;
        public Sprite wheelImage;
    }

    public Info[] wheelInfos;

	void Awake()
	{
        instance = this;
	}

    public void SortWheels()
    {
        Array.Sort(wheelInfos, (a, b) => (string.Compare(a.wheelName.ToString(), b.wheelName.ToString())));
        for (int i = 0; i < wheelInfos.Length; i++)
            wheelInfos[i].wheelNo = i;
    }

}
