﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WheelGridItem : MonoBehaviour
{

    public Text wheelNameText;
    public Image wheelImage;

    [HideInInspector]
    public WheelInfo.Info info;

    void Start()
    {
        this.GetComponent<Button>().onClick.AddListener(() => OnClick());
    }

    public void SetInfo(WheelInfo.Info w)
    {
        info = w;
        wheelNameText.text = info.wheelName;
        wheelImage.sprite = info.wheelImage;
    }

    public void SelectWheel(bool bSelect)
    {
        Color color = this.GetComponent<Image>().color;
        color.a = bSelect ? 1 : 0;
        this.GetComponent<Image>().color = color;
    }

    public void OnClick()
    {
        WheelSelectManager.instance.SetWheel(info.wheelNo, false);
    }

}
