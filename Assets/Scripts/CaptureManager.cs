﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureManager : MonoBehaviour {

    public GameObject fadePanel;

    CaptureAndSave snapShot;

	// Use this for initialization
	void Start () {
        snapShot = GameObject.FindObjectOfType<CaptureAndSave>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    void OnEnable()
    {
        CaptureAndSaveEventListener.onError += OnError;
        CaptureAndSaveEventListener.onSuccess += OnSuccess;
    }

    void OnDisable()
    {
        CaptureAndSaveEventListener.onError -= OnError;
        CaptureAndSaveEventListener.onSuccess -= OnSuccess;
    }

    void OnError(string error)
    {
        Debug.Log("Error : " + error);
    }

    void OnSuccess(string msg)
    {
        Debug.Log("Success : " + msg);
    }

    public void OnCaptureButton()
    {
        StartCoroutine(Do_Capture());
        //fadePanel.GetComponent<Animator>().SetTrigger("fade");
        //snapShot.CaptureAndSaveToAlbum(Screen.width, Screen.height, Camera.main, ImageType.JPG);
    }

    IEnumerator Do_Capture()
    {
        GameObject curObj = WheelManager.Instance.curWheel;
        WheelManager.Instance.SelectWheel(null);

        fadePanel.GetComponent<Animator>().SetTrigger("fade");
        yield return new WaitForSeconds(0.25f);
        snapShot.CaptureAndSaveToAlbum(Screen.width, Screen.height, Camera.main, ImageType.JPG);

        WheelManager.Instance.SelectWheel(curObj);
    }

}
