﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashManager : MonoBehaviour {

    public Image splashImage;

    private float timer = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        timer += Time.deltaTime;
        if (timer < 1f)
            splashImage.color = Color.Lerp(Color.black, Color.white, timer);
        else if (timer < 2f)
            splashImage.color = Color.white;
        else if (timer < 3f)
            splashImage.color = Color.Lerp(Color.white, Color.black, timer - 2f);
        else
            Application.LoadLevel("Main");
	}
}
