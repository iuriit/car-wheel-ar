﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WheelSelectManager : MonoBehaviour
{

    public static WheelSelectManager instance;

    #region Public Variables
    [Header("Wheel Select Panel")]
    public GameObject selectPanel;
    public RectTransform wheelItemContent;
    public GameObject wheelItemPrefab;

    [Header("Wheel Grid Panel")]
    public GameObject gridPanel;
    public Transform wheelGridContent;
    public GameObject wheelGridItemPrefab;

    [Header("Wheel Size")]
    public Text wheelSizeText;

    //[Header("Wheel")]
    //public Wheel[] wheels;

    [HideInInspector]
    public int curWheelNo = 0;
    [HideInInspector]
    public float curWheelSize = 0.5f;
    [HideInInspector]
    public bool isSelect = false, isMove = false;
    #endregion

    #region Private Variables

    // Item Scroll View
    private float ITEM_WIDTH = -250f, MOVE_TIME = 0.25f;
    private Vector3 prevMousePos;
    private float prevMovePos, targetMovePos, moveTimer = 0;

    // Grid Panel Swipe
    private bool isGridPanelMove = false;
    private Vector3 prevSwipeMousePos;

    // Scale Wheel
    private int prevTouchCount = 0;
    private bool isScale = false;
    private float orgDist = 0, orgScale = 0;
    #endregion

    #region Initialization
    void Start()
    {
        instance = this;
        InitWheels();
    }
    #endregion

    #region Update
    void Update()
    {
        ShowWheelSize();
        MoveItem();
        SwipeGridPanel();
        //ScaleWheel();
        prevTouchCount = Input.touchCount;
    }

    void ShowWheelSize()
    {
        if (WheelManager.Instance.curWheel != null)
        {
            curWheelSize = WheelManager.Instance.curWheel.GetComponent<Wheel>().wheelSize;
            wheelSizeText.text = string.Format("Wheel Size: {0:0.0}\"", curWheelSize * 39.3701 - 1.25);
        }
        else
            wheelSizeText.text = "";
    }

    void MoveItem()
    {
        if (isMove)
        {
            moveTimer += Time.deltaTime;
            if (moveTimer > MOVE_TIME)
            {
                isMove = false;
                wheelItemContent.anchoredPosition = new Vector2(GetWheelItemPosition(curWheelNo), 0);
            }
            else
            {
                wheelItemContent.anchoredPosition = new Vector2(Mathf.Lerp(prevMovePos, targetMovePos, (moveTimer / MOVE_TIME) * (moveTimer / MOVE_TIME)), 0);
            }
        }
    }

    void SwipeGridPanel()
    {
        if (Input.touchCount == 1)
        {
            var touch0 = Input.GetTouch(0);
            if (prevTouchCount != Input.touchCount)
            {
                //isGridPanelMoveUp = true;
                prevSwipeMousePos = touch0.position;
            }

            //if(isGridPanelMoveUp)
            {
                float dist = touch0.position.y - prevSwipeMousePos.y;
                if (isGridPanelMove && dist > 100)
                {
                    isGridPanelMove = false;
                    isSelect = true;
                    gridPanel.GetComponent<Animator>().SetBool("grid_up", true);
                }
                else if(isGridPanelMove && dist < -100)
                {
                    isGridPanelMove = false;
                    isSelect = true;
                    gridPanel.GetComponent<Animator>().SetBool("grid_up", false);
                }
            }
        }
        else
        {
            isGridPanelMove = false;
            isSelect = true;
        }
    }

    void ScaleWheel()
    {
        //if (Input.touchCount == 2)
        //{
        //    var touch0 = Input.GetTouch(0);
        //    var touch1 = Input.GetTouch(1);
        //    if (prevTouchCount != Input.touchCount)
        //    {
        //        isScale = true;
        //        orgDist = Vector2.Distance(touch0.position, touch1.position);
        //        orgScale = wheels[0].wheelParent.localScale.x;
        //    }

        //    if (isScale)
        //    {
        //        float dist = Vector2.Distance(touch0.position, touch1.position);
        //        float scale = orgScale * (dist / orgDist);
        //        for (int i = 0; i < wheels.Length; i++)
        //            wheels[i].SetWheelScale(scale);
        //    }
        //}
        //else
            //isScale = false;
    }
    #endregion

    #region Public UI Methods
    public void ShowPanel()
    {
        selectPanel.SetActive(WheelManager.Instance.curWheel != null);
        if(WheelManager.Instance.curWheel != null)
        {
            SetWheel(WheelManager.Instance.curWheel.GetComponent<Wheel>().wheelNo, false);
        }
    }

    public void OnItemPanelPointerDown()
    {
        if (isMove)
            return;
        isSelect = false;
        prevMousePos = Input.mousePosition;
        OnGridPanelSwipePointerDown();
    }

    public void OnItemPanelDrag()
    {
        if (!isSelect && Mathf.Abs(Input.mousePosition.x - prevMousePos.x) > 50)
        {
            if (Input.mousePosition.x - prevMousePos.x > 50)          // Swipe Right
            {
                SelectWheel(-1);
            }
            else if (Input.mousePosition.x - prevMousePos.x < -50)    // Swipe Left
            {
                SelectWheel(1);
            }
        }
    }

    public void OnGridPanelSwipePointerDown()
    {
        isGridPanelMove = true;
    }

    public void OnGridPanelBackButton()
    {
        isGridPanelMove = false;
        isSelect = true;
        gridPanel.GetComponent<Animator>().SetBool("grid_up", false);
    }
    #endregion

    #region Public Helper Methods
    public void InitWheels()
    {
        WheelInfo.instance.SortWheels();

        foreach (Transform child in wheelItemContent)
            GameObject.Destroy(child.gameObject);
        foreach (Transform child in wheelGridContent)
            GameObject.Destroy(child.gameObject);

        AddWheel(WheelInfo.instance.wheelInfos[WheelInfo.instance.wheelInfos.Length - 2]);
        AddWheel(WheelInfo.instance.wheelInfos[WheelInfo.instance.wheelInfos.Length - 1]);
        for (int i = 0; i < WheelInfo.instance.wheelInfos.Length; i++)
        {
            AddWheel(WheelInfo.instance.wheelInfos[i]);
            AddGridWheel(WheelInfo.instance.wheelInfos[i]);
        }
        AddWheel(WheelInfo.instance.wheelInfos[0]);
        AddWheel(WheelInfo.instance.wheelInfos[1]);

        wheelItemContent.anchoredPosition = new Vector2(GetWheelItemPosition(0), 0);
    }

    public void AddWheel(WheelInfo.Info info)
    {
        GameObject obj = GameObject.Instantiate(wheelItemPrefab);
        WheelItem item = obj.GetComponent<WheelItem>();
        item.SetInfo(info);
        if (info.wheelNo == curWheelNo)
            item.wheelNameText.color = Color.yellow;

        obj.transform.SetParent(wheelItemContent);
        obj.transform.localScale = Vector3.one;
        obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, obj.transform.localPosition.y, 0);
    }

    public void AddGridWheel(WheelInfo.Info info)
    {
        GameObject obj = GameObject.Instantiate(wheelGridItemPrefab);
        WheelGridItem item = obj.GetComponent<WheelGridItem>();
        item.SetInfo(info);
        item.SelectWheel(info.wheelNo == curWheelNo);

        obj.transform.SetParent(wheelGridContent);
        obj.transform.localScale = Vector3.one;
        obj.transform.localPosition = new Vector3(obj.transform.localPosition.x, obj.transform.localPosition.y, 0);
    }

    public void SetWheel(int w, bool bMove)
    {
        curWheelNo = w;

        if(WheelManager.Instance.curWheel != null)
            WheelManager.Instance.curWheel.GetComponent<Wheel>().SetWheel(w);

        foreach(Transform child in wheelItemContent)
        {
            WheelItem item = child.GetComponent<WheelItem>();
            item.wheelNameText.color = (item.info.wheelNo == w) ? Color.yellow : Color.white;
        }
        foreach(Transform child in wheelGridContent)
        {
            WheelGridItem item = child.GetComponent<WheelGridItem>();
            item.SelectWheel(item.info.wheelNo == w);
        }

        if(!bMove)
        {
            wheelItemContent.anchoredPosition = new Vector2(GetWheelItemPosition(curWheelNo), 0);
        }
    }

    public float GetWheelItemPosition(int w)
    {
        return ITEM_WIDTH * (w + 1);
    }

    public void SelectWheel(int dir)
    {
        isGridPanelMove = false;
        isSelect = true;
        isMove = true;
        moveTimer = 0;
        prevMovePos = wheelItemContent.anchoredPosition.x;
        targetMovePos = wheelItemContent.anchoredPosition.x + ITEM_WIDTH * dir;
        SetWheel((curWheelNo + WheelInfo.instance.wheelInfos.Length + dir) % WheelInfo.instance.wheelInfos.Length, true);
    }
    #endregion
}