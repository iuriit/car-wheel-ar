﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class WheelItem : MonoBehaviour, IPointerDownHandler,IDragHandler {

    public Text wheelNameText;
    public Image wheelImage;

    [HideInInspector]
    public WheelInfo.Info info;

	void Start()
	{
        this.GetComponent<Button>().onClick.AddListener(() => OnClick());
	}

    public void SetInfo(WheelInfo.Info w)
    {
        info = w;
        wheelNameText.text = info.wheelName;
        wheelImage.sprite = info.wheelImage;
    }

    public void OnClick()
    {
        print(WheelSelectManager.instance.isSelect + "," + WheelSelectManager.instance.isMove);
        if (WheelSelectManager.instance.isSelect || WheelSelectManager.instance.isMove)
            return;
        int v = (info.wheelNo - WheelSelectManager.instance.curWheelNo + WheelInfo.instance.wheelInfos.Length) % WheelInfo.instance.wheelInfos.Length;
        if (v == WheelInfo.instance.wheelInfos.Length - 1)
            WheelSelectManager.instance.SelectWheel(-1);
        else if (v == 1)
            WheelSelectManager.instance.SelectWheel(1);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        WheelSelectManager.instance.OnItemPanelPointerDown();
    }

    public void OnDrag(PointerEventData eventData)
    {
        WheelSelectManager.instance.OnItemPanelDrag();
    }

}
