﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Wheel : MonoBehaviour {

    //public GameObject arrow;
    //public GameObject[] planes;
    public MeshRenderer[] models;
    public GameObject movePlane;

    [HideInInspector]
    public int wheelNo;
    [HideInInspector]
    public float wheelSize;

	public void SetWheelSize(float size)
    {
        wheelSize = size;
        this.transform.localScale = new Vector3(size, size, size);
    }

    public void SetWheel(int w)
    {
        wheelNo = w;
        foreach(MeshRenderer mr in models)
            mr.material.mainTexture = WheelInfo.instance.wheelInfos[wheelNo].wheelImage.texture;
    }

    public void EnableMove(bool bMove)
    {
        movePlane.SetActive(bMove);
    }

    //public void ShowArrow(bool bShow)
    //{
    //    arrow.SetActive(bShow);
    //}

    //public void SelectArrow(int arr)
    //{
    //    for (int i = 0; i < arrow.transform.childCount; i++)
    //    {
    //        arrow.transform.GetChild(i).gameObject.SetActive(i == arr || arr == -1);
    //    }
    //}

    //public void SetPlane(int dir)
    //{
    //    for (int i = 0; i < planes.Length; i++)
    //    {
    //        planes[dir].SetActive(i == dir);
    //    }
    //}
}
