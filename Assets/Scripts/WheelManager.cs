﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.iOS;

public class WheelManager : MonoBehaviour
{
    public static WheelManager Instance;

    #region Public Variables
    public Transform wheelParent;
    public GameObject wheelPrefab;
    public Slider moveSlider;
    public Text moveSliderText;

    [HideInInspector]
    public GameObject curWheel = null;
    #endregion

    #region Private Variables
    // Touch Action
    private int prevTouchCount = 0;
    // Move
    private bool isMove = false;
    private int moveDir = 0;
    private float movePosition, prevXPos;
    private Vector3 prevPosition, prevWheelPosition;

    // Scale
    private bool isScaleRotate = false;
    private Vector2 prevTouch0, prevTouch1;
    private float prevSize, prevAngle;
	#endregion

	#region Initialization
	private void Awake()
	{
        Instance = this;
        moveSlider.onValueChanged.AddListener(OnSliderValueChange);
	}

	void Start()
    {
    }
    #endregion

    #region Update
    void Update()
    {
        UserActions();
    }

    public void UserActions()
    {
        if(Input.touchCount == 1)
        //if(!Input.GetMouseButtonUp(0))
        {
        }
        else
        {
            isMove = false;
            if (curWheel != null)
                curWheel.GetComponent<Wheel>().EnableMove(false);
        }

        if(Input.touchCount == 2)
        {
            var touch0 = Input.GetTouch(0).position;
            var touch1 = Input.GetTouch(1).position;

            if(prevTouchCount != 2 && curWheel != null)
            {
                // Scale & Rotate
                isScaleRotate = true;
                prevTouch0 = touch0;
                prevTouch1 = touch1;
                prevSize = curWheel.GetComponent<Wheel>().wheelSize;
                prevAngle = curWheel.transform.localEulerAngles.y;
            }

            if(isScaleRotate)
            {
                curWheel.GetComponent<Wheel>().SetWheelSize(prevSize * Vector2.Distance(touch0, touch1) / Vector2.Distance(prevTouch0, prevTouch1));
                curWheel.transform.localEulerAngles = new Vector3(0, prevAngle + Vector2.SignedAngle(touch1 - touch0, prevTouch1 - prevTouch0) * 2, 0);
            }
        }
        else
        {
            isScaleRotate = false;
        }

        prevTouchCount = Input.touchCount;
    }
    #endregion

    #region UI Handlers
    public void OnAdd()
    {
        GameObject wheel = GameObject.Instantiate(wheelPrefab);
        wheel.transform.SetParent(wheelParent);
        wheel.transform.localScale = Vector3.one;
        wheel.GetComponent<Wheel>().SetWheel(WheelSelectManager.instance.curWheelNo);
        wheel.GetComponent<Wheel>().SetWheelSize(WheelSelectManager.instance.curWheelSize);

        SelectWheel(wheel);

        Vector3 screenPosition = Camera.main.ScreenToViewportPoint(new Vector3(Screen.width / 2, Screen.height / 3, 0));
        ARPoint point = new ARPoint
        {
            x = screenPosition.x,
            y = screenPosition.y
        };

        ARHitTestResultType[] resultTypes = {
                        ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingExtent, 
                        // if you want to use infinite planes use this:
                        ARHitTestResultType.ARHitTestResultTypeExistingPlane,
                        ARHitTestResultType.ARHitTestResultTypeEstimatedHorizontalPlane, 
                        ARHitTestResultType.ARHitTestResultTypeEstimatedVerticalPlane, 
                        ARHitTestResultType.ARHitTestResultTypeFeaturePoint
                    };

        foreach (ARHitTestResultType resultType in resultTypes)
        {
            if (HitTestWithResultType(wheel.transform, point, resultType))
            {
                return;
            }
        }

        // If not detected
        wheel.transform.position = Camera.main.transform.forward * 4 + Camera.main.transform.position;
        wheel.transform.LookAt(Camera.main.transform.position);
        wheel.transform.localEulerAngles = new Vector3(0, 180 + wheel.transform.localEulerAngles.y, 0);
    }

    public void OnDelete()
    {
        GameObject.Destroy(curWheel);
        SelectWheel(null);
    }

    public void OnPointerDown()
    {
        if(Input.touchCount == 1)
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;
            if(Physics.Raycast(ray, out hit))
            {
                //if(hit.collider.tag == "Arrow")
                //{
                //    isMove = true;
                //    moveDir = hit.collider.GetComponentInParent<Arrow>().ArrowDir;
                //    curWheel.GetComponent<Wheel>().SelectArrow(moveDir);
                //    prevWheelPosition = curWheel.transform.position;
                //    prevPosition = hit.point;
                //}
                if(hit.collider.tag == "Wheel")
                {
                    SelectWheel(hit.collider.gameObject);

                    isMove = true;
                    Wheel wheel = hit.collider.gameObject.GetComponent<Wheel>();
                    wheel.EnableMove(true);
                    prevPosition = hit.point;
                }
            }
            else
            {
                SelectWheel(null);
            }
        }
    }

    public void OnPointerDrag()
    {
        if(Input.touchCount == 1)
        {
            if(isMove)
            {
                //float distance = Vector3.Distance(Camera.main.transform.position, prevPosition);
                //Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                //Vector3 v = ray.GetPoint(distance) - prevPosition;
                //Vector3 w = (moveDir == 0 ? -curWheel.transform.forward : (moveDir == 1 ? curWheel.transform.up : -curWheel.transform.right));
                //Vector3 proj = Vector3.Project(v, w);
                //curWheel.transform.position = prevWheelPosition + proj;

                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit[] hits = Physics.RaycastAll(ray);
                foreach(RaycastHit hit in hits)
                {
                    if(hit.collider.tag == "Plane")
                    {
                        curWheel.transform.position += (hit.point - prevPosition);
                        prevPosition = hit.point;
                        break;
                    }
                }
            }
        }
    }

    public void OnSliderValueChange(float v)
    {
        moveSliderText.text = string.Format("{0:0.00}", moveSlider.value);
        if(curWheel != null)
        {
            curWheel.transform.position += curWheel.transform.forward * (moveSlider.value - prevXPos);
            prevXPos = moveSlider.value;
        }
    }
    #endregion

    #region Helper Methods
    bool HitTestWithResultType(Transform ts, ARPoint point, ARHitTestResultType resultTypes)
    {
        List<ARHitTestResult> hitResults = UnityARSessionNativeInterface.GetARSessionNativeInterface().HitTest(point, resultTypes);
        if (hitResults.Count > 0)
        {
            foreach (var hitResult in hitResults)
            {
                Vector3 pos = UnityARMatrixOps.GetPosition(hitResult.worldTransform);
                pos.y += 0.5f;
                ts.position = pos;
                ts.LookAt(Camera.main.transform.position);
                ts.localEulerAngles = new Vector3(0, 180 + ts.localEulerAngles.y, 0);
                return true;
            }
        }
        return false;
    }

    public void SelectWheel(GameObject wheel)
    {
        if(curWheel != wheel)
        {
            if (curWheel != null)
                curWheel.GetComponent<Wheel>().EnableMove(false);

            curWheel = wheel;
            WheelSelectManager.instance.ShowPanel();
            moveSlider.value = 0;
            prevXPos = 0;
            OnSliderValueChange(0);
        }
    }
    #endregion
}
